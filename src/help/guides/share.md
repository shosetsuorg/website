---
title: Share
lang: en-US
---

# Share

Shosetsu has two methods of sharing content with other users.

1. Sharing the content URL
2. Sharing a Shosetsu QR Code (URL)

Content URLs are the actual URL for a novel. 
Using such a URL makes it difficult to automatically open Shosetsu for a novel, 
providing a subpar experience.

A Shosetsu QR Code (URL) is a special Shosetsu way to sharing content with others. 
In the future, Shosetsu QR Codes will share more then just novels, 
you can share Styles & Repositories with others too!

## Adding share links

Due to complications with the current state of QR Code libraries, 
Shosetsu QR Codes must be read by another app.
You can use your Phones Camera, 
and if that does not support QR Codes, 
you can use a dedicated QR Code reading application.
I (Doomsdayrs) perfer using [QR & Barcode Scanner](https://f-droid.org/en/packages/com.example.barcodescanner/).

Follow these steps once you have an application to read the QR Codes.
1. Scan the Shosetsu QR Code with the app.
2. Depending on the application, you either copy the URL parsed / open in browser and then copy the URL.
3. Take the copied URL code and input it in Shosetsu>More>Add Share Link
4. Press "Apply"
5. Wait, soon you will be displayed the content being shared to you, letting you easily access it in Shosetsu!

## Sharing QR Codes

When viewing the information for a Novel, you can click the share button, and share the QR Code.

Simply screenshot, and send the entire image to who you want to share to!
