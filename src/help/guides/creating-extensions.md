---
title: Creating Extensions
lang: en-US
---

# Creating Extensions

**Shosetsu** currently uses Lua extensions and an API on which the whole app is built off of.

[Lua][lua] is a simple interperated language. 
We recommend skimming through [Programming in Lua][pil] and/or the [reference manual][manual] before trying to start writing extensions.

## Requirements

- An internet connection.
- Basic programming knowledge.
- Any text editor *except Microsoft Notepad*, preferably an IDE.

::: guide IDE Recommendation
For this guide we recommend [IntelliJ IDEA](https://www.jetbrains.com/idea/), as that is the one we've based our guide on.
::: aside
Download [here](https://www.jetbrains.com/idea/download/)
:::

## Setup

1. Fork the [extensions](https://gitlab.com/shosetsuorg/extensions/-/forks/new) repository
2. Clone the repository to your local machine
    ```bash
    git clone https://gitlab.com/USERNAME_HERE/extensions.git
    # For the SSH fans
    git clone git@gitlab.com:USERNAME_HERE/extensions.git
    ```
3. In the directory, run the `dev-setup.sh` script to download development files to your repository.

### IntelliJ IDEA

1. Install the **EmmyLua** and **Kotlin** plugins (**`Settings > Plugins > Marketplace`**) and restart **IDEA**.
2. Open the directory of the extensions repo with **`File > Open`**.
3. Now you can start development!

### Extension testing

#### Android emulation

Best method.

Run Android Studio to run Shosetsu, and be able to see logcat to see output and or errors.

#### Extension tester

Easiest method.

Download the jar from [here][tester] and plop it into the repository directory.

When you need to test something, perform the following command and the tester will test.

```bash
java -jar ./extension-tester.jar src/LANG/EXTENSION.lua
```

## Understanding Lua Extensions and Libraries

Each Lua extension has the responsibility for an individual site, but sites often use available software, which means they share the same code base.
This is why extensions sometimes do not have any code but a call to `Require`.
A good example is the plethora of extensions based on the **Madara** library, which defer all logic to the **Madara** library.

### Kotlin Lib

Shosetsu uses something called the "kotlin-lib" for all its fundamental functionality.

Documentation can be found [here][lib-doc]. 
I suggest looking at the [documentation][lib-doc-lua] of the custom Lua functions you can use.

### Extension information

#### Template

You can acquire a template extension from [here][template]. 
This template has documentation on the various fields expected in an extension.

#### Header

A Commented JSON Header is included at the start of every Lua Extension.
This is used for file based identification.

```json
-- {"id":-1,"ver":"1.0.0","libVer":"1.0.0","author":"","repo":"","dep":["foo","bar"]}
```

#### Life cycle

An extension has a life cycle internally in Shosetsu.

| Stage				| Description
| ----------------- | -------------------------------------------------------------	|
| Initialization	| Extension is required for some process, and is initialized.	|
| Invocation		| Extension is invoked for whatever process needs it.			|
| Storage in memory	| Extension is stored in memory till it is needed again.		|
| Discarded			| Extension is discarded from memory during app shutdown.		|

This means that values you declare in the top level scope of an extension are 
 retained throughout the entire extension lifecycle.

##### Explanation of values

| Name		| Description																													|
| --------- | ----------------------------------------------------------------------------------------------------------------------------- |
| id 		| Unique ID of the extension, should match in all locations.																	|
| ver 		| Version of this extension, should match index. This can be set to a version behind to always appear as an update is needed.	|
| libVer 	| Version of [kotlin-lib][lib] that this extension is designed to work with.													|
| author 	| Your name or user name.																										|
| repo 		| If your extension is not based on the shosetsu extension repository, place your repo url here. Currently does nothing.		|
| dep 		| A list of dependencies that this extension requires. Currently does nothing.													|

### Constants provided

| Name		 | Use									  | Value
| ---------- | -------------------------------------- | -------
| QUERY		 | To retrieve the query data from data.  | 0
| PAGE_INDEX | Index of the page number to start with | 1

### Variable Naming

There are a few DO NOT's with creating extensions.
1. DO NOT name a local or global variable the same as any of the above

## Writing your extension

Now that you understand the basics, you can get to writing your extension:
1. Copy the template you downloaded to `src/LANG/NAME.lua`. LANG being the language of the website and NAME being the file name you want to give it.
2. Fill in required fields.
3. Remove optionals that you do not need.
4. Create functions according to specification
5. Test the extension
6. Repeat 5 until there are no bugs.
7. Make a PR to upstream.

[lua]: https://www.lua.org/about.html
[pil]: https://www.lua.org/pil/contents.html
[manual]: https://www.lua.org/manual/5.1/
[template]: https://gitlab.com/shosetsuorg/kotlin-lib/-/raw/main/templates/extension-template.lua
[lib]: https://gitlab.com/shosetsuorg/kotlin-lib
[lib-doc]: https://docs.shosetsu.app/
[lib-doc-lua]: https://docs.shosetsu.app/shosetsu-kotlin-lib/app.shosetsu.lib.lua/-shosetsu-lua-lib/-lib-functions/index.html
[tester]: https://gitlab.com/shosetsuorg/extension-tester/-/releases/permalink/latest
