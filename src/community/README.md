---
home: true
lang: en-US
sidebar: false
---

# Community

Come join the Shosetsu Community.

Below are our two options to join!

## Matrix

Matrix is our preferred platform, open source just like Shosetsu!

[**Join**](https://matrix.to/#/#shosetsu:matrix.org)

## Discord

We also have a legacy Discord server.

[**Join**](https://discord.gg/ttSX7gB)